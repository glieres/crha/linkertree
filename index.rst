.. index::
   ! CRHA (Citoyens Résistants d'Hier et d'Aujourd'hui)


.. raw:: html

   <a rel="me" href="https://framapiaf.org/@goldman_bakounine"></a>
   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://piaille.fr/@ldh_grenoble"></a>



.. un·e
.. https://framapiaf.org/web/tags/raar.rss
.. https://framapiaf.org/web/tags/golem.rss

|FluxWeb| `RSS <https://glieres.frama.io/crha/linkertree/rss.xml>`_

.. _linkertree:
.. _links:

================================================================================
Liens **CRHA (Citoyens Résistants d'Hier et d'Aujourd'hui)** |crha|
================================================================================

Liens CRHA
=============

- https://twitter.com/CRHA_glieres
- https://glieres.frama.io/crha/resistances
- http://www.citoyens-resistants.fr
- https://www.youtube.com/@CRHA_RassemblementdesGlieres/videos
- https://fr-fr.facebook.com/rassemblementdesglieres/
- https://glieres.frama.io/crha/resistances-2024
- https://glieres.frama.io/crha/resistances-2023
- https://glieres.frama.io/crha/resistances


::

    129 rue des Fleuries , Thorens-Glières, France
    citoyen.2008@yahoo.fr

.. figure:: images/crha_5000.png
   :align: center


.. figure:: images/carte_thorens_glieres.png
   :align: center

   Thorens-Glières, cinéma Le Parnal, monument en mémoire des résistants espagnols, MJC et salle Tom Morel https://www.openstreetmap.org/#map=18/45.99726/6.24842


Comptes twitter
==================

- https://twitter.com/CRHA_glieres
- https://twitter.com/Gilles_Perret
