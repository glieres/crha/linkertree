#
# Configuration file for the Sphinx documentation builder.
# http://www.sphinx-doc.org/en/stable/config

import platform
from datetime import datetime
from zoneinfo import ZoneInfo

import sphinx
import sphinx_material

project = "Liens CRHA"
html_title = project
html_logo = "images/crha_avatar.png"
author = "humain·e"
now = datetime.now(tz=ZoneInfo("Europe/Paris"))
version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"

today=version

extensions = [
    "sphinx.ext.intersphinx",
]

intersphinx_mapping = {
    "guerrieres": ("https://luttes.frama.io/pour/la-paix/les-guerrieres-de-la-paix/", None),
    "chinsky": ("https://judaism.gitlab.io/floriane_chinsky/", None),
    "pal_2023": ("https://conflits.frama.io/israel-palestine/israel-palestine-2023/", None),
    "sionisme": ("https://israel.frama.io/sionisme-antisionisme/", None),
    "antisem": ("https://luttes.frama.io/contre/l-antisemitisme/", None),
    "golem": ("http://collectif-golem.frama.io/golem", None),
}
# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", ".venv"]
html_static_path = ["_static"]
html_show_sourcelink = True
html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}
extensions.append("sphinx_material")
html_theme_path = sphinx_material.html_theme_path()
html_context = sphinx_material.get_html_context()
html_theme = "sphinx_material"

extensions += ["sphinx.ext.todo"]
todo_include_todos = True

# material theme options (see theme.conf for more information)
# https://gitlab.com/bashtage/sphinx-material/blob/master/sphinx_material/sphinx_material/theme.conf
# Colors
# The theme color for mobile browsers. Hex color.
# theme_color = #3f51b5
# Primary colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange,
# brown, grey, blue-grey, white
# Accent colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange
# color_accent = blue
# color_primary = blue-grey

html_theme_options = {
    "base_url": "http://glieres.frama.io/crha/linkertree",
    "repo_url": "https://framagit.org/glieres/crha/linkertree",
    "repo_name": project,
    "html_minify": False,
    "html_prettify": True,
    "css_minify": True,
    "repo_type": "gitlab",
    "globaltoc_depth": -1,
    "color_primary": "deep-purple",
    "color_accent": "blue",
    "theme_color": "red",
    "nav_title": f"{project} ({today})",
    "master_doc": False,
    "nav_links": [
        {
            "href": "genindex",
            "internal": True,
            "title": "Index",
        },
        {
            "href": "https://glieres.frama.io/crha/resistances-2024/",
            "internal": False,
            "title": "CRHA 2024",
        },
        {
            "href": "https://glieres.frama.io/crha/resistances-2023/",
            "internal": False,
            "title": "CRHA 2023",
        },
        {
            "href": "https://glieres.frama.io/crha/resistances/",
            "internal": False,
            "title": "CRHA avant 2023",
        },
        {
            "href": "https://luttes.frama.io/contre/l-antisemitisme/",
            "internal": False,
            "title": "Contre l'antisémitisme",
        },
    ],
    "heroes": {
        "index": "Liens CRHA",
    },
    "table_classes": ["plain"],
}
# https://github.com/sphinx-contrib/yasfb
extensions.append("yasfb")
feed_base_url = html_theme_options["base_url"]
feed_author = "Scribe"
# https://sphinx-design.readthedocs.io/en/furo-theme/get_started.html
extensions.append("sphinx_design")
# https://sphinx-tags.readthedocs.io/en/latest/quickstart.html#installation
extensions.append("sphinx_tags")
tags_create_tags = True
# Whether to display tags using sphinx-design badges.
tags_create_badges = True

language = "en"
html_last_updated_fmt = ""

todo_include_todos = True

html_use_index = True
html_domain_indices = True

copyright = f"2020-{now.year}, {author} Built with sphinx {sphinx.__version__} Python {platform.python_version()}"

rst_prolog = """
.. |crha| image:: /images/crha_avatar.png
.. |FluxWeb| image:: /images/rss_avatar.webp
"""
